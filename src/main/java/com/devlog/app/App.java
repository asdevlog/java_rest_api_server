package com.devlog.app;

import com.devlog.api.Server;
import com.devlog.database.DB;
import com.devlog.database.VModelManager;
import com.devlog.database.VehicleManager;
import com.devlog.domain.VModel;
import com.devlog.domain.Vehicle;
import org.hibernate.SessionFactory;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws ClassNotFoundException, IOException {
        System.out.println( "Create new vehicle" );

        // Hibernate

        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Server.run();


//        VehicleManager veh_manager = new VehicleManager();
//        veh_manager.setSessionFactory(sessionFactory);
//
//        generateModel(sessionFactory);
//        List<Vehicle> veh_list = veh_manager.getAllVehicle();
//        System.out.println( veh_list );
    }
}
