package com.devlog.app;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

    private static SessionFactory  sessionFactory = null;

    static {
        Configuration cfg = new Configuration().configure();
        StandardServiceRegistryBuilder builder= new StandardServiceRegistryBuilder()
                .applySettings(
                cfg.getProperties()
        );
        sessionFactory = cfg.buildSessionFactory(builder.configure().build());

    }

    public static SessionFactory getSessionFactory()  {
        return sessionFactory;
    }
}
