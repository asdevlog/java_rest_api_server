package com.devlog.utils;

import com.devlog.database.VModelManager;
import com.devlog.domain.VModel;
import org.hibernate.SessionFactory;

import java.util.ArrayList;

public class Generator {


    public static void generateModel(SessionFactory sessionFactory) {
        ArrayList<VModel> list_model= new ArrayList<>();
        VModel model1 = new VModel();
        model1.setName("УАЗ");
        VModel model2 = new VModel();
        model2.setName("ГАЗ");
        VModel model3 = new VModel();
        model3.setName("МАЗ");
        list_model.add(model1);
        list_model.add(model2);
        list_model.add(model3);

        VModelManager model_manager = new VModelManager();
        model_manager.setSessionFactory(sessionFactory);
        for (VModel i:list_model) {
            model_manager.addModel(i);
        }
    }

}
