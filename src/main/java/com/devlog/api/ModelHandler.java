package com.devlog.api;

import com.devlog.app.HibernateUtil;
import com.devlog.database.VModelManager;
import com.devlog.domain.VModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.hibernate.SessionFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import static com.devlog.api.Server.splitQuery;

public class ModelHandler implements HttpHandler {
    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        OutputStream os = null;
        String jsonResponse = "";
        String noNameText = "-";
        Map<String, List<String>> params = splitQuery(exchange.getRequestURI().getRawQuery());

        VModelManager vModelManager = new VModelManager();
        vModelManager.setSessionFactory(sessionFactory);

        switch (exchange.getRequestMethod()) {
            case ("GET"):

                List<VModel> allVehicle = vModelManager.getAllModel();

                final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                final ObjectMapper mapper = new ObjectMapper();

                mapper.writeValue(outStream, allVehicle);

                jsonResponse = outStream.toString();
                break;
            case ("POST"):

                String name = params.getOrDefault("name", List.of(noNameText)).stream().findFirst().orElse(noNameText);

                VModel newModel = new VModel();
                newModel.setName(name);

                vModelManager.addModel(newModel);
                jsonResponse = objectMapper.writeValueAsString(newModel);
                break;
            case ("PUT"):
                String id = params.getOrDefault("id", null).stream().findFirst().orElse(null);
                if (id != null) {
                    String updateName = params.getOrDefault("name", List.of(noNameText)).stream().findFirst().orElse(noNameText);
                    VModel updateModel = vModelManager.updateModel(id,updateName);
                    if (updateModel != null)  {
                        jsonResponse = objectMapper.writeValueAsString(updateModel);
                    } else {
                        jsonResponse = "Error update";
                    }
                } else {
                    jsonResponse = "Wrong param";
                }
                break;
            case ("DELETE"):
                String deleteId = params.getOrDefault("id", null).stream().findFirst().orElse(null);
                vModelManager.deleteModel(deleteId);
                jsonResponse = "Delete Done";

        }
        exchange.getResponseHeaders().set("Content-type", "application/json; charset=UTF-8");
        exchange.sendResponseHeaders(200, jsonResponse.getBytes().length);
        os = exchange.getResponseBody();
        os.write(jsonResponse.getBytes());
        os.close();
    }
}
