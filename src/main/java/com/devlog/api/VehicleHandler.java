package com.devlog.api;

import com.devlog.app.HibernateUtil;
import com.devlog.database.VModelManager;
import com.devlog.database.VehicleManager;
import com.devlog.domain.VModel;
import com.devlog.domain.Vehicle;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.hibernate.SessionFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.devlog.api.Server.splitQuery;

public class VehicleHandler implements HttpHandler {
    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        OutputStream os = null;
        String jsonResponse = "";
        String noNameText = "-";
        Map<String, List<String>> params = splitQuery(exchange.getRequestURI().getRawQuery());

        VehicleManager vehicleManager = new VehicleManager();
        VModelManager vmodelManager = new VModelManager();
        vehicleManager.setSessionFactory(sessionFactory);
        vmodelManager.setSessionFactory(sessionFactory);

        switch (exchange.getRequestMethod()) {
            case ("GET"):

                List<Vehicle> allVehicle = vehicleManager.getAllVehicle();

                final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                final ObjectMapper mapper = new ObjectMapper();

                mapper.writeValue(outStream, allVehicle);

                final byte[] resultData = outStream.toByteArray();
                jsonResponse = new String(resultData);
                break;
            case ("POST"):

                String name = params.getOrDefault("name", List.of(noNameText)).stream().findFirst().orElse(noNameText);
                String garageName = params.getOrDefault("garage_name", List.of(noNameText)).stream().findFirst().orElse(noNameText);
                String newVehicleModelId = params.getOrDefault("model_id", null).stream().findFirst().orElse(null);

                VModel newVehicleModel = vmodelManager.getModel(newVehicleModelId);

                Vehicle newVehicle = new Vehicle();
                newVehicle.setName(name);
                newVehicle.setGarage_name(garageName);

                newVehicle.setModel(newVehicleModel);

                vehicleManager.addVehicle(newVehicle);
                jsonResponse = objectMapper.writeValueAsString(newVehicle);
                break;
            case ("PUT"):
                String id = params.getOrDefault("id", null).stream().findFirst().orElse(null);
                if (id != null) {
                    String updateName = params.getOrDefault("name", List.of("")).stream().findFirst().orElse(null);
                    String updateModel = params.getOrDefault("model_id", List.of("")).stream().findFirst().orElse(null);
                    String updateGarageName = params.getOrDefault("garage_name",  List.of("")).stream().findFirst().orElse(null);

                    Vehicle updateVehicle = vehicleManager.updateVehicle(id,updateName,updateGarageName,updateModel);
                    if (updateVehicle != null)  {
                        jsonResponse = objectMapper.writeValueAsString(updateVehicle);
                    } else {
                        jsonResponse = "Error update";
                    }
                } else {
                    jsonResponse = "Wrong param";
                }
                break;
            case ("DELETE"):
                String deleteId = params.getOrDefault("id", null).stream().findFirst().orElse(null);
                vehicleManager.deleteVehicle(deleteId);
                jsonResponse = "Delete Done";

        }
        exchange.getResponseHeaders().set("Content-type", "application/json; charset=UTF-8");
        exchange.sendResponseHeaders(200, jsonResponse.getBytes().length);
        os = exchange.getResponseBody();
        os.write(jsonResponse.getBytes());
        os.close();
    }
}
