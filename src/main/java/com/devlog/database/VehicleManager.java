package com.devlog.database;

import com.devlog.domain.VModel;
import com.devlog.domain.Vehicle;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;
import java.util.UUID;

public class VehicleManager {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Vehicle> getAllVehicle() {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery("from Vehicle", Vehicle.class);
            return query.list();
        }
    }

    public void addVehicle(Vehicle vehicle ) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.persist(vehicle);
            transaction.commit();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public Vehicle updateVehicle(String id, String name, String garage_name, String modelId) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();

            Vehicle updateVehicle = session.get(Vehicle.class, UUID.fromString(id));

            if (name.length() > 0) {updateVehicle.setName(name);}
            if (garage_name.length() > 0) {updateVehicle.setGarage_name(garage_name);}
            if (modelId.length() > 0) {
                VModel requestModel = session.get(VModel.class, UUID.fromString(modelId));
                updateVehicle.setModel(requestModel);
            }

            session.persist(updateVehicle);
            transaction.commit();
            return updateVehicle;
        }  catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public void deleteVehicle(String id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();

            Vehicle updateVehicle = session.get(Vehicle.class, UUID.fromString(id));
            session.remove(updateVehicle);
            transaction.commit();
        }  catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
