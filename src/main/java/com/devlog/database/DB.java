package com.devlog.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class DB {

    public Connection connect() throws ClassNotFoundException {
        Connection conn = null;
        //Class.forName("org.postgresql.Driver");
        try {
//            String url = "jdbc:postgresql://localhost:5454/veh_test";
//            String user = "systerov";
//            String password = "Abdf5dbc";
//            conn = DriverManager.getConnection(url, user, password);
            String url = "jdbc:postgresql://localhost:5454/veh_test?user=systerov&password=Abdf5dbc";
            conn = DriverManager.getConnection(url);
            System.out.println("Connected to the PostgreSQL server successfully.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return conn;
    }
}
