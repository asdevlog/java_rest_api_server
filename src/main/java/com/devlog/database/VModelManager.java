package com.devlog.database;

import com.devlog.domain.VModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;
import java.util.UUID;

public class VModelManager {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<VModel> getAllModel() {
        try (Session session = sessionFactory.openSession()) {
            Query<VModel> query = session.createQuery("from VModel", VModel.class);
            return query.list();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public VModel getModel(String id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(VModel.class, UUID.fromString(id));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public void addModel(VModel model ) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.persist(model);
            transaction.commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public VModel updateModel(String id, String name) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();

            VModel updateModel = session.get(VModel.class, UUID.fromString(id));
            updateModel.setName(name);
            session.persist(updateModel);
            transaction.commit();
            return updateModel;
        }  catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public void deleteModel(String id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();

            VModel updateModel = session.get(VModel.class, UUID.fromString(id));
            session.remove(updateModel);
            transaction.commit();
        }  catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
