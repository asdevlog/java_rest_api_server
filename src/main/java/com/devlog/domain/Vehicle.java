package com.devlog.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

@Entity
@Getter @Setter
@Table(name="vehicle" , schema="public")
public class Vehicle implements Serializable {
    private static final long serialVersionUID = 7855937172997134350L;
    @Id
    @GeneratedValue
    public UUID id;

    public String garage_name;

    public String name;

    @ManyToOne
    @JoinColumn(name="model_id", nullable=true)
    public VModel model;

}
