package com.devlog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
//import com.fasterxml.jackson.
@Entity(name="VModel")
@Getter @Setter
@Table(name = "model", schema = "public")
@JsonIgnoreProperties(value = { "vehicle" }, ignoreUnknown = true)
public class VModel implements Serializable {
    private static final long serialVersionUID = 7855934172997134350L;
    @Id
    @GeneratedValue
    public UUID id;
    public String name;
    @OneToMany(mappedBy = "model", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Vehicle> vehicle = new ArrayList<>();
}
